// detect button clicks
var buttons = document.querySelectorAll(".drum");

for (i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
        playDrumSound(this.innerText);
        keyDownAnimation(this.innerText);
    });
}

// detect keyboard press events
document.addEventListener("keydown", function(event) {
    playDrumSound(event.key);
    keyDownAnimation(event.key);
});

function playDrumSound(drum) {
    switch (drum) {
        case "w":
            var tom1 = new Audio("sounds/tom-1.mp3");
            tom1.play();
            break;
        case "a":
            var tom2 = new Audio("sounds/tom-2.mp3");
            tom2.play();
            break;
        case "s":
            var tom3 = new Audio("sounds/tom-3.mp3");
            tom3.play();
            break;
        case "d":
            var tom4 = new Audio("sounds/tom-4.mp3");
            tom4.play();
            break;
        case "j":
            var snare = new Audio("sounds/snare.mp3");
            snare.play();
            break;
        case "k":
            var crash = new Audio("sounds/crash.mp3");
            crash.play();
            break;
        case "l":
            var kick = new Audio("sounds/kick-bass.mp3");
            kick.play();
        default:
            console.log(this);
    }
}

function keyDownAnimation(currentKey) {
    var pressedKey = document.querySelector("." + currentKey);

    pressedKey.classList.add("pressed");
    setTimeout(function() {
        pressedKey.classList.remove("pressed");
    }, 100);
}